﻿'use strict';
    app.factory('ipregistrationFactory', ["$http","$q", "constants",function ($http,$q, constants) {
        var serviceurl = constants.baseAddress + "Master/";
        return {
            getPatientInfo: function () {
                var dfd = $q.defer();
                var url = constants.IPRegistration;
                $http.get(url).success(function (response) {
                    dfd.resolve(response);
                }).error(function (response) {
                    dfd.reject(response);
                });
                return dfd.promise;
            },
            savePatientInfo: function (input) {
                var deferred = $q.defer();
                var promise = $http({
                    method: 'POST',
                    url: constants.savePatientInfo,
                    data: input
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data, status) {
                    deferred.reject(data);
                });
                return promise;
            },


            getUsersList: function () {
                var dfd = $q.defer();
                var url = serviceurl + "GetUsers";
                $http.get(url).success(function (response) {
                    dfd.resolve(response);
                }).error(function (response) {
                    dfd.reject(response);
                });
                return dfd.promise;
            },
            getUser: function (user) {
                var url = serviceurl + "GetUser/" + user.UserId;
                return $http.get(url);
            },
            addUser: function (user) {
                var url = serviceurl + "AddUser";
                return $http.post(url, user);
            },
            deleteUser: function (user) {
                var url = serviceurl + "DeleteUser/" + user.UserId;
                return $http.delete(url);
            },
            updateUser: function (user) {
                var url = serviceurl + "ModifyUser/" + user.UserId;
                return $http.put(url, user);
            }
        };
    }]);
