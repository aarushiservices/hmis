'use strict';

app.controller('ipregistrationCtrl', ["$scope", "ipregistrationFactory", function ($scope, ipregistrationFactory) {
    $scope.patientInfo = { "AdharNo" : "",
        "Age":"",
        "BloodGroup": "",
        "DOB":"",
        "Emailid": "",
        "EmergencyContact": "9988776655",
        "EmergencyName" : "avatar",
        "EmergencyRelation": "avatar1",
        "FirstName ": "raju",
        "Gender":1,
        "HospitalBranchId":1,
        "LastName": "R",
        "Mobilenumber" : "8899774433",
        "MStatus" : "Unmarrid",
        "Occupation" : "farmer",
        "PatientID" : "1",
        "Title" : "dr",  
        "Address1" : "Hydrabad",
        "Address2" : "Sanathnagar",
        "City": "Rangareddy",
        "HomePhone" : "6677889944",
        "Landmark" : "Near Rsbrothers",
        "PatientID" : "1",
        "Pincode ": "3333",
        "WorkPhone ":"44444444",
        "A_CHolderName":"Rajesh",
        "A_CNumber": "6588487475",
        "BankName": "BOI",
        "CompanyName": "Aarushi",
        "EffictiveFrom": "aarushi1",
        "EffictiveTo": "aarushi2"
    }

    

    var init = function () {        
        ipregistrationFactory.getPatientInfo().then(function (res) {
            console.log(res);
            $scope.patientInfo = res[0];
        }, function (res) {
        });
    };
   // init();

    $scope.savePatientInfo = function(){
        ipregistrationFactory.savePatientInfo($scope.patientInfo).then(function (res) {
            console.log(res);
            $scope.patientInfo = res[0];
        }, function (res) {
        });
    };
}]);

