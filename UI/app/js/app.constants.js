'use strict';

// Declare app level module which depends on views, and components
angular.module('app').constant("constants", {
    IPRegistration: "http://localhost:1921/api/patient/get",
    savePatientInfo: "http://localhost:1921/api/patient/post"
});
