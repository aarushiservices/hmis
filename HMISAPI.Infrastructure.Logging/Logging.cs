﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Diagnostics;
using System.Net.Mail;
using System.Net;
using System.IO;
using HMISAPI.Domain.Interfaces;

namespace HMISAPI.Infrastructure.Logging
{
    public class Logging : ILogging
    {
        public Logging()
        {
        }
        public void Connection()
        {
        }
        ///// <summary>
        ///// Logs the error.
        ///// </summary>
        ///// <param name="ee">The ee.</param>
        ///// <param name="userFriendlyError">The user friendly error.</param>
        ///// <returns></returns>
        //public static string LogError(Exception ee, string userFriendlyError)
        //{
        //    string logType = ConfigurationManager.AppSettings["HMISConnectionString"].ToString();
        //    if (logType.Equals("1"))
        //    {
        //        return LogErrorToLogFile(ee, userFriendlyError);
        //    }
        //    else if (logType.Equals("2"))
        //    {
        //        return LogErrorInSystemEvent(ee, userFriendlyError);
        //    }
        //    else if (logType.Equals("3"))
        //    {
        //        NameValueCollection section = (NameValueCollection)ConfigurationManager.GetSection("MyDictionary");
        //        //SendEmail(toAddress, ccAddress, bccAddress, userFriendlyError, ee.ToString(), true);
        //        return "Mail sent";
        //    }
        //    else
        //    {
        //        LogErrorInSystemEvent(ee, userFriendlyError);
        //        SendEmail("ysa.naidu@gmail.com", "ysa.naidu@outlook.com", "ysa.naidua@gmail.com", userFriendlyError, ee.ToString(), true);
        //        return LogErrorToLogFile(ee, userFriendlyError);
        //    }
        //}
              
        ///// <summary>
        ///// Log the error and return
        ///// </summary>
        ///// <param name="ee">The ee.</param>
        ///// <param name="userFriendlyError">The user friendly error.</param>
        ///// <returns></returns>
        //public static string LogErrorToLogFile(Exception ee, string userFriendlyError)
        //{
        //    try
        //    {
        //        string path = context.Current.Server.MapPath("~/Logging/");
        //        // check if directory exists
        //        if (!Directory.Exists(path))
        //        {
        //            Directory.CreateDirectory(path);
        //        }
        //        path = path + DateTime.Today.ToString("dd-MMM-yy") + ".`";
        //        // check if file exist
        //        if (!File.Exists(path))
        //        {
        //            File.Create(path).Dispose();
        //        }
        //        // log the error now
        //        using (StreamWriter writer = File.AppendText(path))
        //        {
        //            string error = "\r\nLog written at : " + DateTime.Now.ToString() +
        //                           "\r\nError occured on page : " + context.Current.Request.Url.ToString() +
        //                           "\r\n\r\nHere is the actual error :\n" + ee.ToString();
        //            writer.WriteLine(error);
        //            writer.WriteLine("==========================================");
        //            writer.Flush();
        //            writer.Close();
        //        }

        //    }
        //    catch
        //    {
        //        //throw;
        //    }
        //    return userFriendlyError;
        //}

        ///// <summary>
        ///// Logs the error in system event.
        ///// </summary>
        ///// <param name="ee">The ee.</param>
        ///// <param name="userFriendlyError">The user friendly error.</param>
        ///// <returns></returns>
        //public static string LogErrorInSystemEvent(Exception ee, string userFriendlyError)
        //{
        //    try
        //    {
        //        string eventLog = "HMS";
        //        string eventSource = "HMS";

        //        // check if source exists
        //        if (!System.Diagnostics.EventLog.SourceExists(eventSource))
        //        {
        //            System.Diagnostics.EventLog.CreateEventSource(eventSource, eventLog);
        //        }

        //        // create the instance of the EventLog and log the error
        //        using (System.Diagnostics.EventLog myLog = new System.Diagnostics.EventLog(eventLog))
        //        {
        //            myLog.Source = eventSource;

        //            string error = "\r\nLog written at : " + DateTime.Now.ToString() +
        //                           "\r\nError occured on page : " + context.Current.Request.Url.ToString() +
        //                           "\r\n\nHere is the actual error :\n" + ee.ToString();
        //            myLog.WriteEntry(error, EventLogEntryType.Error);
        //        }

        //        return userFriendlyError;
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //}

        //public static bool SendEmail(string toAddress, string ccAddress, string bccAddress, string subject, string body, bool isHtml)
        //{
        //    NameValueCollection EmailSettings = (NameValueCollection)ConfigurationManager.GetSection("EmailSettings");
        //    try
        //    {
        //        using (SmtpClient smtpClient = new SmtpClient())
        //        {
        //            using (MailMessage message = new MailMessage())
        //            {

        //                MailAddress fromAddress = new MailAddress(EmailSettings.GetValues("From").GetValue(0).ToString(), EmailSettings.GetValues("Name").GetValue(0).ToString());
        //                // You can specify the host name or ipaddress of your server
        //                smtpClient.Host = EmailSettings.GetValues("Host").GetValue(0).ToString(); //you can also specify mail server IP address here
        //                //Default port will be 25
        //                smtpClient.Port = 25;
        //                NetworkCredential info = new NetworkCredential(EmailSettings.GetValues("From").GetValue(0).ToString(), EmailSettings.GetValues("Password").GetValue(0).ToString());
        //                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
        //                smtpClient.UseDefaultCredentials = false;
        //                smtpClient.Credentials = info;
        //                //From address will be given as a MailAddress Object
        //                message.From = fromAddress;
        //                //message.Priority = priority;
        //                // To address collection of MailAddress
        //                message.To.Add(toAddress);
        //                message.Subject = subject;
        //                // CC and BCC optional
        //                if (ccAddress.Length > 0)
        //                {
        //                    message.CC.Add(ccAddress);
        //                }
        //                if (bccAddress.Length > 0)
        //                {
        //                    message.Bcc.Add(bccAddress);
        //                }
        //                //Body can be Html or text format
        //                //Specify true if it is html message
        //                message.IsBodyHtml = isHtml;
        //                // Message body content
        //                message.Body = body;
        //                // Send SMTP mail
        //                smtpClient.Send(message);

        //            }
        //        }
        //        return true;
        //    }
        //    catch (Exception ee)
        //    {
        //        string e = ee.ToString();
        //        //System.Net.Mail.MailMessage MailMesaji = new System.Net.Mail.MailMessage();
        //        //MailMesaji.Subject = subject;
        //        //MailMesaji.Body = body;
        //        //MailMesaji.BodyEncoding = Encoding.GetEncoding("Windows-1254"); // Turkish Character Encoding

        //        //MailMesaji.From = new MailAddress(EmailSettings.GetValues("From").GetValue(0).ToString());

        //        //MailMesaji.To.Add(new MailAddress(toAddress));

        //        //System.Net.Mail.SmtpClient Smtp = new SmtpClient();

        //        //Smtp.Host = EmailSettings.GetValues("Host").GetValue(0).ToString(); // for example gmail smtp server

        //        //Smtp.EnableSsl = true;

        //        //Smtp.Credentials = new System.Net.NetworkCredential(EmailSettings.GetValues("From").GetValue(0).ToString(), EmailSettings.GetValues("Password").GetValue(0).ToString());

        //        //Smtp.Send(MailMesaji);
        //        return true;
        //    }
        //}
    }
}
