﻿using HMISAPI.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HMIS.Domain.Entities;
using HMISAPI.Infrastructure.Repository;
using HMIS.Infrastructure.Data;

namespace HMISAPI.Infrastructure.Data
{
    public class Patient : IPatient
    {
        public void RegisterPatient(PatientDetails PobjPatientDetails)
        {
            try
            {
                HMISTenantContext LobjHMISTenantContext = new HMISTenantContext();

                tblPatintDemographic LobjtblPatintDemographic = new tblPatintDemographic();
                tblPatientContactInfo LobjtblPatientContactInfo = new tblPatientContactInfo();
                tblPatientInsurance LobjtblPatientInsurance = new tblPatientInsurance();

                ObjectCopier.CopyObjectData(PobjPatientDetails.PatientDemographics, LobjtblPatintDemographic);
                ObjectCopier.CopyObjectData(PobjPatientDetails.PatientContactInfo, LobjtblPatientContactInfo);
                ObjectCopier.CopyObjectData(PobjPatientDetails.PatientInsurance, LobjtblPatientInsurance);
                LobjtblPatintDemographic.CreatedDate = DateTime.Now;
                
                LobjtblPatintDemographic.tblPatientContactInfo = LobjtblPatientContactInfo;
                LobjtblPatintDemographic.tblPatientInsurance = LobjtblPatientInsurance;
                LobjHMISTenantContext.tblPatintDemographics.Add(LobjtblPatintDemographic);
                //LobjHMISTenantContext.tblPatientContactInfoes.Add(LobjtblPatientContactInfo);
                //LobjHMISTenantContext.tblPatientInsurances.Add(LobjtblPatientInsurance);

                LobjHMISTenantContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
        public void PatientUpdate(PatientDemographics pobjPatientUpdate)
        {
            try
            {
                tblPatintDemographic LobjtblPatintDemographic = new tblPatintDemographic();
                using (var LobjHMISTenantContext = new HMISTenantContext())
                {
                    LobjtblPatintDemographic = LobjHMISTenantContext.tblPatintDemographics.Where(P => P.PatientID == pobjPatientUpdate.PatientID).FirstOrDefault<tblPatintDemographic>();
                    if (LobjtblPatintDemographic != null)
                    {
                        LobjtblPatintDemographic.LastName = pobjPatientUpdate.LastName;
                    }

                    LobjtblPatintDemographic = LobjHMISTenantContext.tblPatintDemographics.Find(pobjPatientUpdate.PatientID);
                    LobjHMISTenantContext.tblPatintDemographics.Remove(LobjtblPatintDemographic);

                    var data = LobjHMISTenantContext.SaveChanges();
                }                
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        public List<PatientDemographics> GetPatientList()
        {
            HMISTenantContext LobjHMISTenantContext = new HMISTenantContext();
            List<PatientDemographics> LlstPatientDemographics = new List<PatientDemographics>();
            
            foreach (tblPatintDemographic LobjtblPatintDemographic in LobjHMISTenantContext.tblPatintDemographics)
            {
                PatientDemographics LobjPatientDemographics = new PatientDemographics();
                ObjectCopier.CopyObjectData(LobjtblPatintDemographic, LobjPatientDemographics);
                LlstPatientDemographics.Add(LobjPatientDemographics);
               // LobjPatientDemographics = null;
            }
            return LlstPatientDemographics;            
        }

        public void PatientDelete(PatientDemographics pobjPatientDelete)
        {
            try
            {
                tblPatintDemographic LobjtblPatintDemographic = new tblPatintDemographic();
                using (var LobjHMISTenantContext = new HMISTenantContext())
                {
                    LobjtblPatintDemographic = LobjHMISTenantContext.tblPatintDemographics.Find(pobjPatientDelete.PatientID);
                    LobjHMISTenantContext.tblPatintDemographics.Remove(LobjtblPatintDemographic);
                    var data = LobjHMISTenantContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void GetPatientDetailsByID(int id)
        {
            try
            {
                PatientDetails LobPatientDetails = new PatientDetails();
                tblPatintDemographic LobjtblPatintDemographic = new tblPatintDemographic();
                tblPatientContactInfo LobtblPatientContactInfo = new tblPatientContactInfo();
                tblPatientInsurance LobtblPatientInsurance = new tblPatientInsurance();
                using (var LobjHMISTenantContext = new HMISTenantContext())
                {
                    LobjtblPatintDemographic = LobjHMISTenantContext.tblPatintDemographics.Find(id);
                    LobtblPatientContactInfo = LobjHMISTenantContext.tblPatientContactInfoes.Find(id);
                    LobtblPatientInsurance = LobjHMISTenantContext.tblPatientInsurances.Find(id);

                  
                    var data = LobjHMISTenantContext.SaveChanges();
                }

               
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
