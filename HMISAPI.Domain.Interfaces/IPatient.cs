﻿using HMISAPI.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMISAPI.Domain.Interfaces
{
    public interface IPatient
    {
        void RegisterPatient(PatientDetails PobjPatientDetails);
        List<PatientDemographics> GetPatientList();
        void PatientUpdate(PatientDemographics pobjpatienupdate);
        void PatientDelete(PatientDemographics pobpatintdelete);
        void GetPatientDetailsByID(int id);

    }
}
