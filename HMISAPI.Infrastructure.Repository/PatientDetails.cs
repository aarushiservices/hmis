﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMISAPI.Infrastructure.Repository
{
    public class PatientDetails
    {
        public PatientDemographics PatientDemographics { get; set; }
        public PatientContactInfo PatientContactInfo { get; set; }
        public PatientInsuranceDetails PatientInsurance { get; set; }
    }

    public class PatientDemographics
    {
        public int PatientID { get; set; }
        public int HospitalBranchId { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobilenumber { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public Nullable<decimal> Age { get; set; }
        public string AdharNo { get; set; }
        public string BloodGroup { get; set; }
        public int Gender { get; set; }
        public string Emailid { get; set; }
        public string MStatus { get; set; }
        public string Occupation { get; set; }
        public string EmergencyName { get; set; }
        public string EmergencyContact { get; set; }
        public string EmergencyRelation { get; set; }        
    }

    public class PatientContactInfo
    {
        public int PatientID { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Location { get; set; }
        public string City { get; set; }
        public string Landmark { get; set; }
        public string Pincode { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
    }

    public class PatientInsuranceDetails
    {
        public int PatientID { get; set; }
        public string InsuranceName { get; set; }
        public string RelationToInsurd { get; set; }
        public Nullable<int> InsuranceID { get; set; }
        public string CompanyName { get; set; }
        public string EffictiveFrom { get; set; }
        public string EffictiveTo { get; set; }
        public string Limit { get; set; }
        public string BankName { get; set; }
        public string IFSCCode { get; set; }
        public string A_CHolderName { get; set; }
        public string A_CNumber { get; set; }
    }
    
    public class Manufacturers
    {
        public int MfgID { get; set; }
        public string MfgName { get; set; }
        public int InventoryType { get; set; }
        public string AgentName { get; set; }
        public string AgentContactNumber { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }

    }

    public class Products
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public int MfgID { get; set; }
        public int TypeID { get; set; }
        public string InventoryType { get; set; }
        public string QuantityPerUnit { get; set; }
        public string LicenceNo { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string RackLocation { get; set; }
        public string Comments { get; set; }
        public string Schedule { get; set; }

    }
    public class ProductType
    {
        public int TypeID { get; set; }
        public string Name { get; set; }
        public int InventoryType { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string Comments { get; set; }
      
    }
        
}
