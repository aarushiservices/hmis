//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HMIS.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblProduct
    {
        public tblProduct()
        {
            this.tblMedicinePurchaseInfoes = new HashSet<tblMedicinePurchaseInfo>();
            this.tblMedicineInvoiceInfoes = new HashSet<tblMedicineInvoiceInfo>();
            this.tblMedicineSalesReturnInfoes = new HashSet<tblMedicineSalesReturnInfo>();
            this.tblMedicineStockReturnInfoes = new HashSet<tblMedicineStockReturnInfo>();
        }
    
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public int MfgID { get; set; }
        public int TypeID { get; set; }
        public string InventoryType { get; set; }
        public string QuantityPerUnit { get; set; }
        public string LicenceNo { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string RackLocation { get; set; }
        public string Comments { get; set; }
        public string Schedule { get; set; }
    
        public virtual tblManufacturer tblManufacturer { get; set; }
        public virtual ICollection<tblMedicinePurchaseInfo> tblMedicinePurchaseInfoes { get; set; }
        public virtual ICollection<tblMedicineInvoiceInfo> tblMedicineInvoiceInfoes { get; set; }
        public virtual tblProductType tblProductType { get; set; }
        public virtual ICollection<tblMedicineSalesReturnInfo> tblMedicineSalesReturnInfoes { get; set; }
        public virtual ICollection<tblMedicineStockReturnInfo> tblMedicineStockReturnInfoes { get; set; }
    }
}
