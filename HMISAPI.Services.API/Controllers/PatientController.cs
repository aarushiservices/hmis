﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HMISAPI.Infrastructure.Repository;
using HMISAPI.Infrastructure.Data;
using System.Web.Http.Cors;

namespace HMISAPI.Services.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PatientController : ApiController
    {
        // GET: api/Patient
        //Content-Type: application/json
        public List<PatientDemographics> Get()
        {
            Patient ObjPatientDetails = new Patient();
           return ObjPatientDetails.GetPatientList();            //return 
           // return new string[] { "value1", "value2" };
        }

        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}
        // GET: api/Patient/5
        public void Get(int PintPID)
        {
            //PatientDetails lobPatientDetails=new PatientDetails();
            //lobPatientDetails.PatientDemographics.PatientID = 5;
            Patient Lobjpatientgetbyid = new Patient();
            Lobjpatientgetbyid.GetPatientDetailsByID(PintPID);
        }

        // POST: api/Patient
        public void Post([FromBody]PatientDetails PobjPatientDetails)
        {
            PatientDetails LobjPatientDetails = new PatientDetails();

            PatientDemographics LobjPatintDemographics = new PatientDemographics();
            LobjPatintDemographics.AdharNo = "ADH50005";
            LobjPatintDemographics.Age = 30;
            LobjPatintDemographics.BloodGroup = "A+";
            LobjPatintDemographics.DOB = Convert.ToDateTime("2014-01-01");
            LobjPatintDemographics.Emailid = "aarushi@gmail.com";
            LobjPatintDemographics.EmergencyContact = "9988776655";
            LobjPatintDemographics.EmergencyName = "avatar";
            LobjPatintDemographics.EmergencyRelation = "avatar1";
            LobjPatintDemographics.FirstName = "raju";
            LobjPatintDemographics.Gender = 1;
            LobjPatintDemographics.HospitalBranchId = 1;
            LobjPatintDemographics.LastName = "R";
            LobjPatintDemographics.Mobilenumber = "8899774433";
            LobjPatintDemographics.MStatus = "Unmarrid";
            LobjPatintDemographics.Occupation = "farmer";
            LobjPatintDemographics.PatientID = 1;
            LobjPatintDemographics.Title = "dr";            
            LobjPatientDetails.PatientDemographics = LobjPatintDemographics;

            PatientContactInfo LobPatientContactInfo = new PatientContactInfo();
            LobPatientContactInfo.Address1 = "Hydrabad";
            LobPatientContactInfo.Address2 = "Sanathnagar";
            LobPatientContactInfo.City = "Rangareddy";
            LobPatientContactInfo.HomePhone = "6677889944";
            LobPatientContactInfo.Landmark = "Near Rsbrothers";
            LobPatientContactInfo.PatientID = 1;
            LobPatientContactInfo.Pincode = "3333";
            LobPatientContactInfo.WorkPhone = "44444444";
            LobjPatientDetails.PatientContactInfo = LobPatientContactInfo;

            PatientInsuranceDetails LobPatientInsuranceDetails = new PatientInsuranceDetails();
            LobPatientInsuranceDetails.A_CHolderName = "Rajesh";
            LobPatientInsuranceDetails.A_CNumber = "6588487475";
            LobPatientInsuranceDetails.BankName = "BOI";
            LobPatientInsuranceDetails.CompanyName = "Aarushi";
            LobPatientInsuranceDetails.EffictiveFrom = "aarushi1";
            LobPatientInsuranceDetails.EffictiveTo = "aarushi2";
            LobjPatientDetails.PatientInsurance = LobPatientInsuranceDetails;

            Patient LobjPatient = new Patient();
            LobjPatient.RegisterPatient(LobjPatientDetails);
        }




        // PUT: api/Patient/5
        public void Put([FromBody]PatientDemographics objPatientDemographics)
        {
        PatientDemographics objUpdate=new PatientDemographics();
        objUpdate.LastName = "ss";
        objUpdate.PatientID = 4;
        Patient lobUpdate = new Patient();
        lobUpdate.PatientUpdate(objUpdate);
        }

        // DELETE: api/Patient/5
        public void Delete([FromBody]PatientDemographics objPatientDemographics)
        {
            PatientDemographics objDelete = new PatientDemographics();
            objDelete.PatientID = 4;
            Patient lobDelete = new Patient();
            lobDelete.PatientDelete(objDelete);
        }       


    }
}
